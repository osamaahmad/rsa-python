from KeysGenerator import KeysGenerator
from Encoder import Encoder


def split(string, max_len):
    return [int(string[i:i + max_len]) for i in range(0, len(string), max_len)]


class RSAEncryptor:

    def __init__(self, generator=KeysGenerator(), encoder=Encoder()):
        self.generator = generator
        self.encoder = encoder

        # if the split number has one less digit,
        # then definitely they're smaller than the mod.
        # NOTE: this will make it not work if the mod
        # size is 1. This case shouldn't happen anyways.
        self.mod_len = len(str(generator.mod())) - 1

    def encrypt(self, plain_message):
        plain_message = self.encoder.string_to_num(plain_message)
        plain_message = self.encoder.normalize(str(plain_message), self.mod_len)
        messages = split(plain_message, self.mod_len)
        for i in range(len(messages)):
            messages[i] = pow(messages[i], self.public_key(), self.mod())
        return messages

    def decrypt(self, encrypted_messages):
        result = ''
        messages_len = len(encrypted_messages)
        for i in range(messages_len):
            message = encrypted_messages[i]
            message = pow(message, self.private_key(), self.mod())
            message = str(message)
            message = self.encoder.normalize(message, self.mod_len)
            result += message
        return self.encoder.num_to_string(int(result))

    def encrypt_to_text(self, plain_message):
        return [self.encoder.num_to_string(message) for message in self.encrypt(plain_message)]

    def decrypt_text(self, encrypted_messages):
        return self.decrypt([self.encoder.string_to_num(message) for message in encrypted_messages])

    def mod(self):
        return self.generator.mod()

    def public_key(self):
        return self.generator.public_key()

    def private_key(self):
        return self.generator.private_key()


