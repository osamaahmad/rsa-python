try:
    from Crypto.Util import number
except ModuleNotFoundError:
    import pip
    pip.main(['install', 'pycryptodome'])
finally:
    from Crypto.Util import number


class KeysGenerator:

    def __init__(self, n_length=2048):
        assert(n_length % 2 == 0)
        assert(n_length > 16)  # this is because of the fixed public key
        self._mod = -1
        self._public_key = -1
        self._private_key = -1
        self.n_length = n_length

    def generate(self):
        prime_length = self.n_length // 2
        p = number.getPrime(prime_length)
        q = number.getPrime(prime_length)
        self._mod = p * q
        totient = (p - 1) * (q - 1)
        self._public_key = 65537
        self._private_key = pow(self._public_key, -1, totient)

    def mod(self):
        if self._mod == -1:
            self.generate()
        return self._mod

    def public_key(self):
        if self._public_key == -1:
            self.generate()
        return self._public_key

    def private_key(self):
        if self._private_key == -1:
            self.generate()
        return self._private_key
