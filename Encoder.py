class Encoder:

    BASE = 10
    char_len = 3
    multiplier = BASE**char_len

    @staticmethod
    def normalize(string, length=char_len):
        mod = len(string) % length
        if mod != 0:
            return '0' * (length - mod) + string
        return string

    @staticmethod
    def string_to_num(string):
        result = ''
        for char in string:
            result += Encoder.normalize(str(ord(char)))
        return int(result)

    @staticmethod
    def num_to_string(num):
        result = ''
        char_len = Encoder.char_len
        string = Encoder.normalize(str(num))

        for i in range(0, len(string), char_len):
            char_int = int(string[i:i+char_len])
            result += chr(char_int)

        return result
