from RSAEncryptor import RSAEncryptor
from KeysGenerator import KeysGenerator
from Encoder import Encoder


def keys_generator_example(generator=KeysGenerator()):
    print("Mod:", generator.mod())
    print("Public Key:", generator.public_key())
    print("Private Key:", generator.private_key())


def encoder_example(message="Hello there!"):
    encoder = Encoder()
    message = encoder.num_to_string(encoder.string_to_num(message))
    print(message)


def encryptor_example(message="Hello there!", n_length=2048):
    generator = KeysGenerator(n_length)
    keys_generator_example(generator)
    encryptor = RSAEncryptor(generator=generator)

    print()
    encrypted = encryptor.encrypt_to_text(message)
    print("Encrypted Message:")
    print(encrypted)

    print()
    decrypted = encryptor.decrypt_text(encrypted)
    print("Decrypted Message:")
    print(decrypted)


lorem_ipsum = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."\
              " Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"\
              " when an unknown printer took a galley of type and scrambled it to make a type"\
              " specimen book. It has survived not only five centuries, but also the leap into"\
              " electronic typesetting, remaining essentially unchanged. It was popularised in"\
              " the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,"\
              " and more recently with desktop publishing software like Aldus PageMaker including"\
              " versions of Lorem Ipsum."

encryptor_example(message=lorem_ipsum)
